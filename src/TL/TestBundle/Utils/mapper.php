<?php

namespace TL\TestBundle\Utils;

use TL\TestBundle\Entity\orders;

class mapper
{
    protected $doctrine;
    protected $lengowtest;
    protected $url_orders;
    
    public function __construct($doctrine, $lengowtest)
    {
        $this->doctrine = $doctrine;   
        $this->lengowtest = $lengowtest;
    }
    public function saveData()
    {        
        $em = $this->doctrine->getManager();
        $repository = $this->doctrine->getRepository('TLTestBundle:orders');
        $xml = $this->lengowtest->getXml();
        $ordersXml = simplexml_load_string($xml);
        
        foreach ($ordersXml->orders->order as $orders){
            $order = new orders;
            $order->setOrderId($orders->order_id->__toString());
            $order->setMarketPlace($orders->marketplace->__toString());
            $order->setOrderPurchaseDate(date_format(date_create($orders->order_purchase_date->__toString()),"Y-m-d"));
            $order->setOrderAmount(floatval($orders->order_amount->__toString()));
            $order->setNbOrders(intval($orders->cart->nb_orders->__toString()));
            $order->setIdProductLengow(intval($orders->cart->idLengow->__toString()));
            
            if(count($repository->findBy(array('orderId' => $order->getOrderId()))) == 0)
            {
                $em->persist($order);
                $em->flush();
            }
        }
        return 'Exit success';
    }
    
    public function getAllData()
    {
        $repository = $this->doctrine->getRepository('TLTestBundle:orders');
        return $repository->findAll();
    }
    
    public function getOrderById($id)
    {
        $repository = $this->doctrine->getRepository('TLTestBundle:orders');
        return $repository->findById($id);
    }
}