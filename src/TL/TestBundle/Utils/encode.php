<?php

namespace TL\TestBundle\Utils;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class encode
{
    public function getJson($orders)
    {
        $normalizers = array(new ObjectNormalizer());        
        $encoders = array(new JsonEncoder());            
        $serializer = new Serializer($normalizers, $encoders);            
        $donnees = $serializer->serialize($orders, 'json');
        
        return ($donnees);
    }
}