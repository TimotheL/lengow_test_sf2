<?php

namespace TL\TestBundle\Utils;

use Monolog\logger;

class lengow_test
{
    protected $logger;
    protected $url_orders;
    
    public function __construct(Logger $logger, $url_orders)
    {
        $this->logger = $logger;
        $this->url_orders = $url_orders;
    }
    public function getXml()
    {
        $xml = file_get_contents($this->url_orders);
        $this->logger->info('téléchargemen du fichier xml');
        return $xml;
    }
}