<?php

namespace TL\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * orders
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TL\TestBundle\Entity\ordersRepository")
 */
class orders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="orderId", type="string", length=255)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255)
     */
    private $marketplace;

    /**
     * @var string
     *
     * @ORM\Column(name="order_purchase_date", type="date", length=255)
     */
    private $orderPurchaseDate;

    /**
     * @var float
     *
     * @ORM\Column(name="order_amount", type="float")
     */
    private $orderAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_orders", type="integer")
     */
    private $nbOrders;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_lengow", type="integer")
     */
    private $idProductLengow;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return orders
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set orderPurchaseDate
     *
     * @param date $orderPurchaseDate
     * @return orders
     */
    public function setOrderPurchaseDate($orderPurchaseDate)
    {
        $this->orderPurchaseDate = $orderPurchaseDate;

        return $this;
    }

    /**
     * Get orderPurchaseDate
     *
     * @return date 
     */
    public function getOrderPurchaseDate()
    {
        return $this->orderPurchaseDate;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return orders
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set nbOrders
     *
     * @param integer $nbOrders
     * @return orders
     */
    public function setNbOrders($nbOrders)
    {
        $this->nbOrders = $nbOrders;

        return $this;
    }

    /**
     * Get nbOrders
     *
     * @return integer 
     */
    public function getNbOrders()
    {
        return $this->nbOrders;
    }

    /**
     * Set idProductLengow
     *
     * @param integer $idProductLengow
     * @return orders
     */
    public function setIdProductLengow($idProductLengow)
    {
        $this->idProductLengow = $idProductLengow;

        return $this;
    }

    /**
     * Get idProductLengow
     *
     * @return integer 
     */
    public function getIdProductLengow()
    {
        return $this->idProductLengow;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return orders
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}
