<?php

namespace TL\TestBundle\Controller;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\YamlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use TL\TestBundle\Entity\orders;
use APY\DataGridBundle\Grid\Source\Entity;
use TL\TestBundle\Form\ordersType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $reponse = $this->container->get('tl_test.mapper')->saveData();
        $source = new Entity('TLTestBundle:orders');
        $grid = $this->get('grid');
        $grid->setSource($source);
        return $grid->getGridResponse('TLTestBundle:orders:listOrders.html.twig');
    }
    
    public function addAction(Request $request)
    {
        $orders = new orders();
        $form = $this->get('form.factory')->create(new ordersType(), $orders);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($orders);
            $em->flush();            
        }
        return $this->render('TLTestBundle:orders:add.html.twig', array('form' => $form->createView()));
    }
    
    public function OrdersOnJsonAction($format)
    {
        $orders = $this->container->get('tl_test.mapper')->getALlData();
        $donnees = $this->container->get('tl_test.encode')->getJson($orders);
        if($format === 'yaml')
        {
            $donnees = Yaml::dump(json_decode($donnees, true));
        }
        
        return $this->render('TLTestBundle:Default:donnees.html.twig', array('format' => $format, 'donnees' => $donnees));
    }
    
    public function OrderOnJsonAction($idOrders, $format)
    {
        $orders = $this->container->get('tl_test.mapper')->getOrderById($idOrders);
        $donnees = $this->container->get('tl_test.encode')->getJson($orders);
        if($format === 'yaml')
        {
            $donnees = Yaml::dump(json_decode($donnees, true));
        }
        return $this->render('TLTestBundle:Default:donnees.html.twig', array('format' =>$format,'donnees' => $donnees));
    }
}
