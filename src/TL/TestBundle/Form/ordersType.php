<?php

namespace TL\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ordersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orderId', 'number')
            ->add('marketplace',    'text')
            ->add('orderPurchaseDate',  'date')
            ->add('orderAmount',    'number')
            ->add('nbOrders',   'number')
            ->add('idProductLengow',    'text')
            ->add('ajouter', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TL\TestBundle\Entity\orders'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tl_testbundle_orders';
    }
}
